# Automatically generated aport, do not edit!
# Generator: pmbootstrap aportgen gcc-aarch64
# Based on: main/gcc (from Alpine)

CTARGET_ARCH=aarch64
CTARGET="$(arch_to_hostspec ${CTARGET_ARCH})"
LANG_D=false
LANG_OBJC=false
LANG_JAVA=false
LANG_GO=false
LANG_FORTRAN=false
LANG_ADA=false
options="!strip"

# abuild doesn't try to tries to install "build-base-$CTARGET_ARCH"
# when this variable matches "no*"
BOOTSTRAP="nobuildbase"

# abuild will only cross compile when this variable is set, but it
# needs to find a valid package database in there for dependency
# resolving, so we set it to /.
CBUILDROOT="/"

_cross_configure="--disable-bootstrap --with-sysroot=/usr/$CTARGET"

pkgname=gcc-aarch64
_pkgbase=13.1.1 # must match gcc/BASE-VER
_pkgsnap=20230520
pkgver=${_pkgbase}_git$_pkgsnap
[ "$BOOTSTRAP" = "nolibc" ] && pkgname="gcc-pass2"
[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""
[ "$CHOST" != "$CTARGET" ] && _target="-$CTARGET_ARCH" || _target=""

pkgname=gcc-aarch64
pkgrel=0
pkgdesc="Stage2 cross-compiler for aarch64"
url="https://gcc.gnu.org"
arch="x86_64"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
_gccrel=$pkgver-r$pkgrel
depends="binutils-aarch64 mpc1"
makedepends_build="gcc g++ bison flex texinfo gawk zip gmp-dev mpfr-dev mpc1-dev zlib-dev"
makedepends_host="linux-headers gmp-dev mpfr-dev mpc1-dev isl-dev zlib-dev musl-dev-aarch64 binutils-aarch64"
subpackages="g++-aarch64:gpp libstdc++-dev-aarch64:libcxx_dev"
[ "$CHOST" = "$CTARGET" ] && subpackages="gcc-gdb gcc-doc$_target"
replaces="libstdc++ binutils"

: "${LANG_CXX:=true}"
: "${LANG_D:=true}"
: "${LANG_OBJC:=true}"
: "${LANG_GO:=true}"
: "${LANG_FORTRAN:=true}"
: "${LANG_ADA:=true}"
: "${LANG_JIT:=true}"

_libgomp=true
_libgcc=false
_libatomic=true
_libitm=true

if [ "$CHOST" != "$CTARGET" ]; then
	if [ "$BOOTSTRAP" = nolibc ]; then
		LANG_CXX=false
		LANG_ADA=false
		_libgcc=false
		_builddir="$srcdir/build-cross-pass2"
	else
		_builddir="$srcdir/build-cross-final"
	fi
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false
	LANG_JIT=false
	_libgomp=false
	_libatomic=false
	_libitm=false

	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# reset target flags (should be set in crosscreate abuild)
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/}"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/}"
	unset CPPFLAGS
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	STRIP_FOR_TARGET="$CTARGET-strip"
elif [ "$CBUILD" != "$CHOST" ]; then
	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/}"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/}"
	unset CPPFLAGS

	# reset flags and cc for build
	export CC_FOR_BUILD="gcc"
	export CXX_FOR_BUILD="g++"
	export CFLAGS_FOR_BUILD=" "
	export CXXFLAGS_FOR_BUILD=" "
	export LDFLAGS_FOR_BUILD=" "
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	# Languages that do not need bootstrapping
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false
	LANG_JIT=false

	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build-cross-native"
else
	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build"

	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# pass -g0 by default to bypass -g, since we don't do debug
	# if -dbg added, the -g is appended and overrides this
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/} -O2"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/} -O2"
	unset CPPFLAGS
	# https://gcc.gnu.org/install/build.html
	export CFLAGS_FOR_TARGET="$CFLAGS"
	export CXXFLAGS_FOR_TARGET="$CXXFLAGS"
	export LDFLAGS_FOR_TARGET="$LDFLAGS"
	export BOOT_CFLAGS="$CFLAGS"
	export BOOT_CXXFLAGS="$CXXFLAGS"
	export BOOT_LDFLAGS="$LDFLAGS"
fi

case "$CARCH" in
# GDC hasn't been ported to PowerPC
# See libphobos/configure.tgt in GCC sources for supported targets
# riscv fails with: error: static assert  "unimplemented"
ppc64le|riscv64)	LANG_D=false ;;
# GDC does currently not work on 32-bit musl architectures.
# This is a known upstream issue.
# See: https://github.com/dlang/druntime/pull/3383
armhf|armv7|x86)	LANG_D=false ;;
esac

# libitm has TEXTRELs in ARM build, so disable for now
case "$CTARGET_ARCH" in
arm*)		_libitm=false ;;
mips*)		_libitm=false ;;
riscv64)	_libitm=false ;;
esac

# Internal libffi fails to build on MIPS at the moment, need to
# investigate further.  We disable LANG_GO on mips64 as it requires
# the internal libffi.
case "$CTARGET_ARCH" in
mips*)		LANG_GO=false ;;
esac

# Fortran uses libquadmath if toolchain has __float128
# currently on x86, x86_64 and ia64
_libquadmath=$LANG_FORTRAN
case "$CTARGET_ARCH" in
x86 | x86_64 | ppc64le)	_libquadmath=$LANG_FORTRAN ;;
*)		_libquadmath=false ;;
esac

# libatomic is a dependency for openvswitch
$_libatomic && subpackages="$subpackages libatomic::$CTARGET_ARCH"
$_libgcc && subpackages="$subpackages libgcc::$CTARGET_ARCH"
$_libquadmath && subpackages="$subpackages libquadmath::$CTARGET_ARCH"
if $_libgomp; then
	depends="$depends libgomp=$_gccrel"
	subpackages="$subpackages libgomp::$CTARGET_ARCH"
fi

case "$CARCH" in
riscv64)
LANG_ADA=false;;
esac

_languages=c
if $LANG_CXX; then
	_languages="$_languages,c++"
fi
if $LANG_D; then
	subpackages="$subpackages libgphobos::$CTARGET_ARCH gcc-gdc$_target:gdc"
	_languages="$_languages,d"
	makedepends_build="$makedepends_build libucontext-dev gcc-gdc-bootstrap"
fi
if $LANG_OBJC; then
	subpackages="$subpackages libobjc::$CTARGET_ARCH gcc-objc$_target:objc"
	_languages="$_languages,objc"
fi
if $LANG_GO; then
	subpackages="$subpackages libgo::$CTARGET_ARCH gcc-go$_target:go"
	_languages="$_languages,go"
fi
if $LANG_FORTRAN; then
	subpackages="$subpackages libgfortran::$CTARGET_ARCH gfortran$_target:gfortran"
	_languages="$_languages,fortran"
fi
if $LANG_ADA; then
	subpackages="$subpackages gcc-gnat$_target:gnat"
	_languages="$_languages,ada"
	if [ "$CBUILD" = "$CTARGET" ]; then
		makedepends_build="$makedepends_build gcc-gnat-bootstrap"
		subpackages="$subpackages libgnat-static:libgnatstatic:$CTARGET_ARCH libgnat::$CTARGET_ARCH"
	else
		subpackages="$subpackages libgnat::$CTARGET_ARCH"
		makedepends_build="$makedepends_build gcc-gnat gcc-gnat$_cross"
	fi
fi
if $LANG_JIT; then
	subpackages="$subpackages libgccjit:jit libgccjit-dev:jitdev"
fi
makedepends="$makedepends_build $makedepends_host"

# when using upstream releases, use this URI template
# https://gcc.gnu.org/pub/gcc/releases/gcc-${_pkgbase:-$pkgver}/gcc-${_pkgbase:-$pkgver}.tar.xz
#
# right now, we are using a git snapshot. snapshots are taken from gcc.gnu.org/pub/gcc/snapshots.
# However, since they are periodically deleted from the GCC mirrors the utilized snapshots are
# mirrored on dev.alpinelinux.org. Please ensure that the snapshot Git commit (as stated in the
# README) matches the base commit on the version-specific branch in the Git repository below.
#
# PLEASE submit all patches to gcc to https://gitlab.alpinelinux.org/kaniini/alpine-gcc-patches,
# so that they can be properly tracked and easily rebased if needed.
source="https://dev.alpinelinux.org/archive/gcc/${_pkgbase%%.*}-$_pkgsnap/gcc-${_pkgbase%%.*}-$_pkgsnap.tar.xz
	0001-posix_memalign.patch
	0002-gcc-poison-system-directories.patch
	0003-specs-turn-on-Wl-z-now-by-default.patch
	0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
	0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
	0006-Enable-Wformat-and-Wformat-security-by-default.patch
	0007-Enable-Wtrampolines-by-default.patch
	0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
	0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
	0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
	0011-libiberty-copy-PIC-objects-during-build-process.patch
	0012-libgcc_s.patch
	0013-nopie.patch
	0014-ada-fix-shared-linking.patch
	0015-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
	0016-add-fortify-headers-paths.patch
	0017-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
	0018-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
	0019-aarch64-disable-multilib-support.patch
	0020-s390x-disable-multilib-support.patch
	0021-ppc64-le-disable-multilib-support.patch
	0022-x86_64-disable-multilib-support.patch
	0023-riscv-disable-multilib-support.patch
	0024-always-build-libgcc_eh.a.patch
	0025-ada-libgnarl-compatibility-for-musl.patch
	0026-ada-musl-support-fixes.patch
	0027-configure-Add-enable-autolink-libatomic-use-in-LINK_.patch
	0028-configure-fix-detection-of-atomic-builtins-in-libato.patch
	0029-libstdc-do-not-throw-exceptions-for-non-C-locales-on.patch
	0030-gdc-unconditionally-link-libgphobos-against-libucont.patch
	0031-druntime-link-against-libucontext-on-all-platforms.patch
	0032-libgnat-time_t-is-always-64-bit-on-musl-libc.patch
	"

# we build out-of-tree
_gccdir="$srcdir"/gcc-${_pkgbase%%.*}-$_pkgsnap
_gcclibdir="/usr/lib/gcc/$CTARGET/${_pkgbase:-$pkgver}"
_gcclibexec="/usr/libexec/gcc/$CTARGET/${_pkgbase:-$pkgver}"

prepare() {
	cd "$_gccdir"

	_err=
	for i in $source; do
		case "$i" in
		*.patch)
			msg "Applying $i"
			patch -p1 -i "$srcdir"/$i || _err="$_err $i"
			;;
		esac
	done

	if [ -n "$_err" ]; then
		error "The following patches failed:"
		for i in $_err; do
			echo "  $i"
		done
		return 1
	fi

	echo ${_pkgbase:-$pkgver} > gcc/BASE-VER
}

build() {
	local _arch_configure=
	local _libc_configure=
	local _bootstrap_configure=
	local _symvers=
	local _jit_configure=

	cd "$_gccdir"

	case "$CTARGET" in
	aarch64-*-*-*)		_arch_configure="--with-arch=armv8-a --with-abi=lp64";;
	armv5-*-*-*eabi)	_arch_configure="--with-arch=armv5te --with-tune=arm926ej-s --with-float=soft --with-abi=aapcs-linux";;
	armv6-*-*-*eabihf)	_arch_configure="--with-arch=armv6kz --with-tune=arm1176jzf-s --with-fpu=vfpv2 --with-float=hard --with-abi=aapcs-linux";;
	armv7-*-*-*eabihf)	_arch_configure="--with-arch=armv7-a --with-tune=generic-armv7-a --with-fpu=vfpv3-d16 --with-float=hard --with-abi=aapcs-linux --with-mode=thumb";;
	mips-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	mips64-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mips64el-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mipsel-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	powerpc-*-*-*)		_arch_configure="--enable-secureplt --enable-decimal-float=no";;
	powerpc64*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no --enable-targets=powerpcle-linux";;
	i486-*-*-*)		_arch_configure="--with-arch=i486 --with-tune=generic --enable-cld";;
	i586-*-*-*)		_arch_configure="--with-arch=i586 --with-tune=generic --enable-cld";;
	s390x-*-*-*)		_arch_configure="--with-arch=z196 --with-tune=zEC12 --with-zarch --with-long-double-128 --enable-decimal-float";;
	riscv64-*-*-*)		_arch_configure="--with-arch=rv64gc --with-abi=lp64d --enable-autolink-libatomic";;
	esac

	case "$CTARGET_ARCH" in
	mips*)	_hash_style_configure="--with-linker-hash-style=sysv" ;;
	*)	_hash_style_configure="--with-linker-hash-style=gnu" ;;
	esac

	case "$CTARGET_LIBC" in
	musl)
		# musl does not support libsanitizer
		# alpine musl provides libssp_nonshared.a, so we don't need libssp either
		_libc_configure="--disable-libssp --disable-libsanitizer"
		_symvers="--disable-symvers"
		export libat_cv_have_ifunc=no
		;;
	esac


	case "$BOOTSTRAP" in
	nolibc)	_bootstrap_configure="--with-newlib --disable-shared --enable-threads=no" ;;
	*)	_bootstrap_configure="--enable-shared --enable-threads --enable-tls" ;;
	esac

	$_libgomp	|| _bootstrap_configure="$_bootstrap_configure --disable-libgomp"
	$_libatomic	|| _bootstrap_configure="$_bootstrap_configure --disable-libatomic"
	$_libitm	|| _bootstrap_configure="$_bootstrap_configure --disable-libitm"
	$_libquadmath	|| _arch_configure="$_arch_configure --disable-libquadmath"

	msg "Building the following:"
	echo ""
	echo "  CBUILD=$CBUILD"
	echo "  CHOST=$CHOST"
	echo "  CTARGET=$CTARGET"
	echo "  CTARGET_ARCH=$CTARGET_ARCH"
	echo "  CTARGET_LIBC=$CTARGET_LIBC"
	echo "  languages=$_languages"
	echo "  arch_configure=$_arch_configure"
	echo "  libc_configure=$_libc_configure"
	echo "  cross_configure=$_cross_configure"
	echo "  bootstrap_configure=$_bootstrap_configure"
	echo "  hash_style_configure=$_hash_style_configure"
	echo ""

	local version="Alpine $pkgver"
	local gccconfiguration="
		--prefix=/usr
		--mandir=/usr/share/man
		--infodir=/usr/share/info
		--build=$CBUILD
		--host=$CHOST
		--target=$CTARGET
		--enable-checking=release
		--disable-cet
		--disable-fixed-point
		--disable-libstdcxx-pch
		--disable-multilib
		--disable-nls
		--disable-werror
		$_symvers
		--enable-__cxa_atexit
		--enable-default-pie
		--enable-default-ssp
		--enable-languages=$_languages
		--enable-link-serialization=2
		--enable-linker-build-id
		$_arch_configure
		$_libc_configure
		$_cross_configure
		$_bootstrap_configure
		--with-bugurl=https://gitlab.alpinelinux.org/alpine/aports/-/issues
		--with-system-zlib
		$_hash_style_configure
		"

	mkdir -p "$_builddir"
	cd "$_builddir"
	"$_gccdir"/configure $gccconfiguration \
		--with-pkgversion="$version"

	msg "building gcc"
	make

	# we build gccjit separate to not build all of gcc with --enable-host-shared
	# as doing so slows it down a few %, so for some quick if's here we gain
	# free performance
	if $LANG_JIT; then
		mkdir -p "$_builddir"/libgccjit-build
		cd "$_builddir"/libgccjit-build
		"$_gccdir"/configure $gccconfiguration \
			--disable-bootstrap \
			--enable-host-shared \
			--enable-languages=jit \
			--with-pkgversion="$version"

		msg "building libgccjit"
		make all-gcc
	fi
}

package() {
	cd "$_builddir"
	make DESTDIR="$pkgdir" install

	ln -s gcc "$pkgdir"/usr/bin/cc

	if $LANG_JIT; then
		make -C "$_builddir"/libgccjit-build/gcc DESTDIR="$pkgdir" jit.install-common
	fi

	# we dont support gcj -static
	# and saving 35MB is not bad.
	find "$pkgdir" \( -name libgtkpeer.a \
		-o -name libgjsmalsa.a \
		-o -name libgij.a \) \
		-delete

	# strip debug info from some static libs
	find "$pkgdir" \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		-o -name libgphobos.a -o -name libgdruntime.a \
		-o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		-o -name libitm.a -o -name libgo.a -o -name libcaf\*.a \
		-o -name libatomic.a -o -name libasan.a -o -name libtsan.a \) \
		-a -type f \
		-exec $STRIP_FOR_TARGET -g {} +

	if $_libgomp; then
		mv "$pkgdir"/usr/lib/libgomp.spec "$pkgdir"/$_gcclibdir
	fi
	if $_libitm; then
		mv "$pkgdir"/usr/lib/libitm.spec "$pkgdir"/$_gcclibdir
	fi

	# remove ffi
	rm -f "$pkgdir"/usr/lib/libffi* "$pkgdir"/usr/share/man/man3/ffi*
	find "$pkgdir" -name 'ffi*.h' -delete

	local gdblib=${_target:+$CTARGET/}lib
	if [ -d "$pkgdir"/usr/$gdblib/ ]; then
		for i in $(find "$pkgdir"/usr/$gdblib/ -type f -maxdepth 1 -name "*-gdb.py"); do
			mkdir -p "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib
			mv "$i" "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib/
		done
	fi

	# move ada runtime libs
	if $LANG_ADA; then
		for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.so"); do
			mv "$i" "$pkgdir"/usr/lib/
			ln -s ../../../../${i##*/} $i
		done
		if [ "$CHOST" = "$CTARGET" ]; then
			for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.a"); do
				mv "$i" "$pkgdir"/usr/lib/
				ln -s ../../../../${i##*/} $i
			done
		fi
	fi

	if [ "$CHOST" != "$CTARGET" ]; then
		# cross-gcc: remove any files that would conflict with the
		# native gcc package
		rm -rf "$pkgdir"/usr/bin/cc "$pkgdir"/usr/include "${pkgdir:?}"/usr/share
		# libcc1 does not depend on target, don't ship it
		rm -rf "$pkgdir"/usr/lib/libcc1.so*

		# fixup gcc library symlinks to be linker scripts so
		# linker finds the libs from relocated sysroot
		for so in "$pkgdir"/usr/"$CTARGET"/lib/*.so; do
			if [ -h "$so" ]; then
				local _real=$(basename "$(readlink "$so")")
				rm -f "$so"
				echo "GROUP ($_real)" > "$so"
			fi
		done
	else
		# add c89/c99 wrapper scripts
		cat >"$pkgdir"/usr/bin/c89 <<'EOF'
#!/bin/sh
_flavor="-std=c89"
for opt; do
	case "$opt" in
	-ansi|-std=c89|-std=iso9899:1990) _flavor="";;
	-std=*) echo "$(basename $0) called with non ANSI/ISO C option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		cat >"$pkgdir"/usr/bin/c99 <<'EOF'
#!/bin/sh
_flavor="-std=c99"
for opt; do
	case "$opt" in
	-std=c99|-std=iso9899:1999) _flavor="";;
	-std=*) echo "$(basename $0) called with non ISO C99 option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		chmod 755 "$pkgdir"/usr/bin/c?9

		# install lto plugin so regular binutils may use it
		mkdir -p "$pkgdir"/usr/lib/bfd-plugins
		ln -s /$_gcclibexec/liblto_plugin.so "$pkgdir/usr/lib/bfd-plugins/"
	fi
}

libatomic() {
	pkgdesc="GCC Atomic library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libatomic.so.* "$subpkgdir"/usr/lib/
}

libcxx() {
	pkgdesc="GNU C++ standard runtime library"
	depends=

	if [ "$CHOST" = "$CTARGET" ]; then
		# verify that we are using clock_gettime rather than doing direct syscalls
		# so we dont break 32 bit arches due to time64.
		nm -D "$pkgdir"/usr/lib/libstdc++.so.* | grep clock_gettime
	fi

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libstdc++.so.* "$subpkgdir"/usr/lib/
}

libcxx_dev() {
	pkgdesc="GNU C++ standard runtime library (development files)"
	depends=
	replaces="g++"

	amove usr/${_target:+$CTARGET/}lib/libstdc++.a \
		usr/${_target:+$CTARGET/}lib/libstdc++exp.a \
		usr/${_target:+$CTARGET/}lib/libstdc++.so \
		usr/${_target:+$CTARGET/}lib/libstdc++fs.a \
		usr/${_target:+$CTARGET/}lib/libsupc++.a \
		usr/${_target:+$CTARGET/}include/c++
}

gpp() {
	pkgdesc="GNU C++ standard library and compiler"
	depends="libstdc++=$_gccrel libstdc++-dev$_target=$_gccrel gcc$_target=$_gccrel libc-dev"
	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/${_target:+$CTARGET/}include \
		"$subpkgdir"/usr/${_target:+$CTARGET/}lib \

	mv "$pkgdir/$_gcclibexec/cc1plus" "$subpkgdir/$_gcclibexec/"

	mv "$pkgdir"/usr/bin/*++ "$subpkgdir"/usr/bin/
}

jit() {
	pkgdesc="GCC JIT Library"
	depends=
	amove usr/lib/libgccjit.so*
}

jitdev() {
	pkgdesc="GCC JIT Library (development files)"
	depends="libgccjit"
	amove usr/include/libgccjit*.h
}

libobjc() {
	pkgdesc="GNU Objective-C runtime"
	replaces="objc"
	depends=
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libobjc.so.* "$subpkgdir"/usr/lib/
}

objc() {
	pkgdesc="GNU Objective-C"
	replaces="gcc"
	depends="libc-dev gcc=$_gccrel libobjc=$_gccrel"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include \
		"$subpkgdir"/usr/lib
	mv "$pkgdir/$_gcclibexec/cc1obj" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/objc "$subpkgdir"/$_gcclibdir/include/
	mv "$pkgdir"/usr/lib/libobjc.so "$pkgdir"/usr/lib/libobjc.a \
		"$subpkgdir"/usr/lib/
}

libgcc() {
	pkgdesc="GNU C compiler runtime libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgcc_s.so.* "$subpkgdir"/usr/lib/
}

libgomp() {
	pkgdesc="GCC shared-memory parallel programming API library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgomp.so.* "$subpkgdir"/usr/lib/
}

libgphobos() {
	pkgdesc="D programming language standard library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgdruntime.so.* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.so.*  "$subpkgdir"/usr/lib/
}

gdc() {
	pkgdesc="GCC-based D language compiler"
	depends="gcc=$_gccrel libgphobos=$_gccrel musl-dev"
	depends="$depends libucontext-dev"
	provides="gcc-gdc-bootstrap=$_gccrel"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include/d/ \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	# Copy: The installed '.d' files, the static lib, the binary itself
	# The shared libs are part of 'libgphobos' so one can run program
	# without installing the compiler
	mv "$pkgdir/$_gcclibexec/d21" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/d/* "$subpkgdir"/$_gcclibdir/include/d/
	mv "$pkgdir"/usr/lib/libgdruntime.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgdruntime.so "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.so "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.spec "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/$CTARGET-gdc "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/gdc "$subpkgdir"/usr/bin/
}

libgo() {
	pkgdesc="Go runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgo.so.* "$subpkgdir"/usr/lib/
}

go() {
	pkgdesc="GCC Go frontend (intended for bootstrapping community/go)"
	depends="gcc=$_gccrel libgo=$_gccrel !go"
	install="$pkgname-go.post-install"

	# See https://lists.alpinelinux.org/~alpine/devel/%3C33KG0XO61I4IL.2Z7RTAZ5J3SY6%408pit.net%3E
	provides="go-bootstrap"
	provider_priority=1 # lowest, see community/go

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/go "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/*gccgo "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/*go "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gofmt "$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/go1 "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/cgo "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/buildid "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/test2json "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/vet "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/usr/lib/libgo.a \
		"$pkgdir"/usr/lib/libgo.so \
		"$pkgdir"/usr/lib/libgobegin.a \
		"$pkgdir"/usr/lib/libgolibbegin.a \
		"$subpkgdir"/usr/lib/
}

libgfortran() {
	pkgdesc="Fortran runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgfortran.so.* "$subpkgdir"/usr/lib/
}

libquadmath() {
	replaces="gcc"
	pkgdesc="128-bit math library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquadmath.so.* "$subpkgdir"/usr/lib/
}

gfortran() {
	pkgdesc="GNU Fortran Compiler"
	depends="gcc=$_gccrel libgfortran=$_gccrel"
	$_libquadmath && depends="$depends libquadmath=$_gccrel"
	replaces="gcc"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gfortran "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgfortran.a \
		"$pkgdir"/usr/lib/libgfortran.so \
		"$subpkgdir"/usr/lib/
	if $_libquadmath; then
		mv "$pkgdir"/usr/lib/libquadmath.a \
			"$pkgdir"/usr/lib/libquadmath.so \
			"$subpkgdir"/usr/lib/
	fi
	mv "$pkgdir"/$_gcclibdir/finclude "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/$_gcclibexec/f951 "$subpkgdir"/$_gcclibexec
	mv "$pkgdir"/usr/lib/libgfortran.spec "$subpkgdir"/$_gcclibdir
}

libgnat() {
	pkgdesc="GNU Ada runtime shared libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.so "$subpkgdir"/usr/lib/
}

libgnatstatic() {
	pkgdesc="GNU Ada static libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.a "$subpkgdir"/usr/lib/
}

gnat() {
	pkgdesc="Ada support for GCC"
	depends="gcc=$_gccrel"
	provides="$pkgname-gnat-bootstrap=$_gccrel"
	[ "$CHOST" = "$CTARGET" ] && depends="$depends libgnat=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/*gnat* "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibdir/*ada* "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/usr/bin/*gnat* "$subpkgdir"/usr/bin/
}

gdb() {
	pkgdesc="$pkgdesc (gdb printers)"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	amove \
		usr/share/gdb/python/ \
		usr/share/gcc-*/python/
}

sha512sums="
d677cb9ca786128ecd0e3fd465e4ba7beb93244dcf174260e3d980f22807aa8df631954a403ff00bc1ed0597bfbd5475d8369c169c44ef1cb69166ed42bfa29e  gcc-13-20230520.tar.xz
a39a2485fcffb05757e8a9100985a4c87cc472028ad756e1992747d3e737bd7526d2cc054f7f0e7c24708d2ba9e898eaebf3f2db424a1895daf796e126772e5e  0001-posix_memalign.patch
a7bf2f69dc983c87dcc480b7c03d80d26128c89afbfea7f035694d7076aacf9361d39b734de5353b52e4e448e84aac68812006da42664bae661b66d4f8ea5098  0002-gcc-poison-system-directories.patch
a0d4f99e0b3bc0c653d9e23b91c602d1e1ce22d67731aaf76ee267654fcffdfdfc2982b4e99ef6b5523def4cfd5a96e2a20aa5f9efa5256315e824dac3bab806  0003-specs-turn-on-Wl-z-now-by-default.patch
ae94863ffecb30a40f3fd79f8b31106bbf8a17dbf13c6036847f1ba2726cc96f2a957f4694f3c880c06ee1dc8fbd990e957b9b55b0275e7d8cc7df10286942fa  0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
2356813aec1ff0a3e82fedfe5dbd0f8c6d442cf567851db40077f98d659b7d048a5349e1ac9c056384a663dad08782d80e29675a457c503200b274a50a6db72d  0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
c9ee725e69b20f15029920bc187178fbb1c6f0375f16ea1a1052187e591569618c93b108b0f4c387530d976e5daba607b9b76e5ec14a776799324ce7f7c3b11d  0006-Enable-Wformat-and-Wformat-security-by-default.patch
70fe923321aa50bd4bf9b67d3bb872e03ecd5dd9669e783d7893513c0c86aaca6cac39c7dcc5818bfd4e5ba59f7e2b2e790ac87fcb0dc348c543c15bbb3f7064  0007-Enable-Wtrampolines-by-default.patch
fa3106eb878ea48479f3145c837b4bb3e563edb9e0535b833f5d7006507af7427902b45c56751c8220eb828e02219e4c54835aa4c0c6eebf797bc29d3d9a7036  0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
8054b3fb3b9e53000fb00229675b6408defac71f38fceff78372e154990540a23b1790cd7b30a61bfd2ae982cc7c9c8c0405398173abd5618e1814d3ccd85ce9  0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
1591ba8c8e873d3e38da61038df0b0da0594f15dd8659c1a2a3f834d55599fc7adab18713c26bcfe389a023dc12d7e00231c4b6b61f6b23ff602c8360d459115  0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
4d03d0bcb31e7a9e171ad408514ec4b1f90783891345bfbf65e513798271b1bbca41a7da616a4ae947a6ba1343a0df9b95233fa202eec70e88fec485841f2413  0011-libiberty-copy-PIC-objects-during-build-process.patch
62574f57043a24ebc644dd761aa46334071c6f0e721375a83f36099b2514dcc7df0b2900fcd52dee7754664e3f59f7686d8b39bf9df5c61893206f34f485cb49  0012-libgcc_s.patch
342782bdaaed8107758345fddb01be3df9e880af235f40c598d46a35c9b1499b7ca10d968f381af168886500aca5a155175d1576f1c30a00555da7467b9d1ccc  0013-nopie.patch
e65bae2cce0e17586384b100ef3dbde232d9f7809cfd40dedb466a24ff2d87b92d68b1a02fe29ec958561bad49658c6a474eae32dd2ee151715808cb5bbfd1a9  0014-ada-fix-shared-linking.patch
d23962505d7bfa99180edfe727df80e4b5d101697537d818327d9e836e90305b16de95729c70268150b55268d01ad5f1d92b70a0a056884f70abf5c4d5870e27  0015-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
a4621dca1edb35e2648a348240bd6790e49b9db1fe7b9f81777222d4b1b241b3ee56a4fca9a02964a00ccc4bd291e37d5911fd142d55002424cd9b9e39f7f98f  0016-add-fortify-headers-paths.patch
3ab96307b31a45e40e174986d13bba620f91954738d84f02ed024854a2cabebf5e6e62589c7006097eb4fb76d5ec526589a877cb8c01f90e5b24819fb191c182  0017-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
da8b091796740d315f2e23cc63820f1f5ed242f5ce04acf1603fa308b6f3e09088b6976a984180ff54cefdfa4b279706e1ef5df675c0644028fbf3c948ee6e37  0018-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
39fec69162c86160ee24030c9f14303dc8aac3341f1a8eabb23d1db58c63a3e7252b5b19172ace6730064a9bf1a848d83882f59b4ada319de46dc99ffbd30c83  0019-aarch64-disable-multilib-support.patch
da2da38d118213567cfbe7c81a8c60deff8e77306b4d5e44d208dfcb6ebc7f29c3964106ebf3dd130edb8a343cc7ecddb3314c14488a429301dbf4750edc65ea  0020-s390x-disable-multilib-support.patch
d74ba03a6da1bcc18e70f017616ab2a1b901e8d04dcb6ea994420aecd042378ec440652ba1168b05a40ddff431ae5b69a19148e928fa70e3e15ba91a03224fad  0021-ppc64-le-disable-multilib-support.patch
828281a2e7404eedfd7a9a90a553ca7e4eab2a19d2a442faa69c3c2ecefe1a3750426bf5fe6d421dedbfba83feddbba04cb6938658d15110b4007813319713c0  0022-x86_64-disable-multilib-support.patch
c7d76f6d987e51e488e404ca75a4c44ed35be6fda925cf0f4cfc9d85e71df6b034d514fdd2748e26d889f8ee30a252224096241348cd06516f2469a3ed6a6a35  0023-riscv-disable-multilib-support.patch
42d9cce946d51d046a96930335915c927d2bb846d6cff685351f58a1006eb5f831b0fd928091df17ed5d4ac1efbbd359b54c325c40e70737d0302d22c8262606  0024-always-build-libgcc_eh.a.patch
c45c449e5304a7483b1141f87a53ab4658402dec8fa5d5116fa7a5e4bebba322da3214cf85c9b25e1c6d040ec81c0094b56a1ed4eefbcde91235816aa0159c6e  0025-ada-libgnarl-compatibility-for-musl.patch
e09ef88df90c4b484ab4172d7ac68c1defd9f1897fadfcb23572e2829e0f795f3eeb1e5ceeb6aac76cab938a0962371e99c168dcf1c2c4c31c8a8b4fa774dc3e  0026-ada-musl-support-fixes.patch
0661fe9d2a61d4de8d9862d6d936bad8236f9ce679b5e40eea7d54b042a7f860f21103e489a8c7a2b12b1e368194ffeb9b4672d7086ebf4189c77f0a27f27a71  0027-configure-Add-enable-autolink-libatomic-use-in-LINK_.patch
68da13272861a0b3f33022350135660af9a060d03529b610dddf540fb77050647934f1a463c681d1556fa0005b6c16fe76cad285667d1349d5be50ccc83c530e  0028-configure-fix-detection-of-atomic-builtins-in-libato.patch
f638c4d7a3fb54d5fd76b7e9b49d8dd1041347409f842453bf862586e2770d595aa9e58f8c1ec45e3c37235d5e74b7327f4610a3cad81c0a757a1271e5a0e572  0029-libstdc-do-not-throw-exceptions-for-non-C-locales-on.patch
f6f2971f4e1bc58c0aeb2a62d9ad551a0fe855b9086f308dce2bb713214415c9679e8e6547290b1514dfd3bc7296d3dfdb7c2964289bbec737984a7810e5ef5d  0030-gdc-unconditionally-link-libgphobos-against-libucont.patch
ab26344341f5e75673e3a95590fb59fde3ee681949c3004e93ef04af9f1fe2d063c22fc21d14882808d3331ce9eeb135a33b87aeed4d3bf74cc9b087ac56610e  0031-druntime-link-against-libucontext-on-all-platforms.patch
5dfdd7645ba5090ac46e025ce3ce629cbf612518654dced2159e0a621d0ef8e231514bad4746aa8863e544077502d4842486b978a0c3857c4ad48b20c49ef2d1  0032-libgnat-time_t-is-always-64-bit-on-musl-libc.patch
"
